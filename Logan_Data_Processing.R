library(plyr); library(dplyr); library(tidyverse); library(raster); library(tidyselect);library(sqldf); library(data.table)
  
age<-read.csv("data/SA2_age_sex_ABS_2016.csv")
colnames(age)[1]<-"SA2_NAME16"
age_logan<-age%>% filter (SA2_NAME16 %in% c( "Jimboomba","Marsden","Regents Park - Heritage Park", 
                                              "Crestmead","Rochedale South - Priestdale","Woodridge",
                                              "Eagleby","Greenbank","Loganholme - Tanah Merah","Boronia Heights - Park Ridge",
                                              "Kingston (Qld.)","Loganlea","Slacks Creek","Bethania - Waterford",
                                              "Shailer Park","Edens Landing - Holmview","Browns Plains",
                                              "Hillcrest","Springwood","Waterford West","Underwood","Beenleigh",
                                              "Cornubia - Carbrook","Logan Central","Daisy Hill","Wolffdene - Bahrs Scrub",
                                              "Logan Village","Mount Warren Park", "Greenbank Military Camp",
                                              "Chambers Flat - Logan Reserve","Munruben - Park Ridge South"))
write.csv(age_logan, file="Logan_SA2_Pop_Age_Sex.csv")

logan_pop <- age_logan %>%
  rowwise() %>%
  mutate(total_pop_sa2 = sum(Total_age_Male,Total_age_Female)) 

tot_pop_logan_sa2<-logan_pop%>%
  group_by(SA2_NAME16)%>%
  summarise(sum_pop=sum(total_pop_sa2))%>%
  mutate(pct=sum_pop/sum(sum_pop)*100)%>%
  mutate(perc_text=paste0(round(pct),"%"))

write.csv(tot_pop_logan_sa2, file="Logan_SA2_Pop.csv")


#weekly income
income<-read.csv("data/Total_income_sa2_Aus.csv")
income_logan<-income%>% filter (SA2_NAME16 %in% c( "Jimboomba","Marsden","Regents Park - Heritage Park", 
                                             "Crestmead","Rochedale South - Priestdale","Woodridge",
                                             "Eagleby","Greenbank","Loganholme - Tanah Merah","Boronia Heights - Park Ridge",
                                             "Kingston","Loganlea","Slacks Creek","Bethania - Waterford",
                                             "Shailer Park","Edens Landing - Holmview","Browns Plains",
                                             "Hillcrest","Springwood","Waterford West","Underwood","Beenleigh",
                                             "Cornubia - Carbrook","Logan Central","Daisy Hill","Wolffdene - Bahrs Scrub",
                                             "Logan Village","Mount Warren Park", 
                                             "Chambers Flat - Logan Reserve","Munruben - Park Ridge South"))

income_logan$Income<-as.character(income_logan$Income)
income_logan$Income<-as.numeric(income_logan$Income)
income_logan$Earners<-as.character(income_logan$Earners)
income_logan$Earners<-as.numeric(income_logan$Earners)


income_logan$inc_pp<-income_logan$Income/income_logan$Earners
income_logan$inc_pw<-income_logan$inc_pp/52

income_logan$SA2_NAME16  <- revalue(income_logan$SA2_NAME16 , c("Kingston"="Kingston (Qld.)"))

write.csv(income_logan, file="weekly_income_Logan")


max(income_logan$inc_pw)
mean(income_logan$inc_pw)
median(income_logan$inc_pw)
min(income_logan$inc_pw)


#PHIDU DATA

pha<-read.csv("data/Phidu_data_Qld_PHA.csv")
logan_pha<-pha%>% filter (PHA_NAME %in% c("Beenleigh/ Eagleby","Bethania - Waterford/ Loganlea/ Waterford West","Boronia Heights - Park Ridge",
                                           "Browns Plains/ Crestmead/ Marsden","Chambers Flat/ Munruben - Park Ridge South",
                                           "Cornubia - Carbrook/ Shailer Park","Daisy Hill/ Springwood","Daisy Hill/ Springwood",
                                           "Edens Landing/ Mount Warren Park/ Wolffdene area","Greenbank","Hillcrest/ Regents Park - Heritage Park",
                                           "Jimboomba/ Logan Village","Kingston/ Slacks Creek","Logan Central/ Woodridge","Loganholme - Tanah Merah",
                                           "Rochedale South - Priestdale/ Underwood"))

logan_pha$Num_Deaths_suicide_Self_injury_2011_2015<-gsub("\\#", "0", logan_pha$Num_Deaths_suicide_Self_injury_2011_2015)
logan_pha$Num_pregnancies_2012_2014 <-gsub(",", "", logan_pha$Num_pregnancies_2012_2014)
logan_pha$Births_2012_2014 <-gsub(",", "", logan_pha$Births_2012_2014)
logan_pha$Num_ppl_18._could_raise_2000_in_a_wk_2014 <-gsub(",", "", logan_pha$Num_ppl_18._could_raise_2000_in_a_wk_2014)
logan_pha$Num_ppl_18._Gov_Support_Main_Source_Income_last_2_yrs_2014 <-gsub(",", "", logan_pha$Num_ppl_18._Gov_Support_Main_Source_Income_last_2_yrs_2014)
logan_pha$Num_ppl_18._who_can_get_support_ppl_outside_household_in_time_crisis_2014 <-gsub(",", "", logan_pha$Num_ppl_18._who_can_get_support_ppl_outside_household_in_time_crisis_2014 )
logan_pha$Num_ppl_18._who_felt_discriminated_last_12_months_2014 <-gsub(",", "", logan_pha$Num_ppl_18._who_felt_discriminated_last_12_months_2014)
logan_pha$Num_ppl_18._who_disagree_with_accepting_other_cultures <-gsub(",", "", logan_pha$Num_ppl_18._who_disagree_with_accepting_other_cultures)
logan_pha$Num_ppl_felt_safe_walking_alone_in_local_area_after_dark_2014 <-gsub(",", "", logan_pha$Num_ppl_felt_safe_walking_alone_in_local_area_after_dark_2014 )
logan_pha$Num <-gsub(",", "", logan_pha$Births_2012_2014)

write.csv(logan_pha, file="Logan_PHA_data.csv")
varname<-switch(input$pha,
                "Num_ppl_18+_who_disagree_with_accepting_other_cultures"="Cultural accpetance",                                                                                                                                                                                             "Electorate Population"="CED_pop_total",
                "Num_ppl_felt_safe_walking_alone_in_local_area_after_dark_2014"="Perceieved safety in local area after dark",
                "Num_Deaths_suicide_Self_injury_2011_2015"="Deaths by suicide & self-harm",
                "Num_pregnancies_2012_2014"="Pregnancies",
                "Births_2012_2014"="Births",
                "Pct_low_birth_weight_babies_2012_2014"="Low birth-weight babies",
                "Pct_smoking_pregnancy_2012_2014"="Smoking during pregnancy",
                "Pct_children_imm_age1_2017"="% fully immunized: age 1",
                "Pct_children_imm_age5_2017"="% fully immunized: age 5")


leafletProxy("map", data = pha_shp_data) %>% clearControls() %>%
  addLegend(pal = pal_pha, opacity = 0.9, title = varname,
            values = ~pha_shp_data[[input$pha]],labels = c(min(input$pha), max(input$pha)),
            position = "bottomright")

observe({
  if (input$pha == "Num_Deaths_suicide_Self_injury_2011_2015") {
    pal_pha <- colorNumeric(c("#ffccff","#330066"), domain= pha_shp_data[[input$pha]])
  } else if (input$pha == "Pct_smoking_pregnancy_2012_2014") {
    pal_pha <- colorNumeric(c("#C12525","navy"), domain= pha_shp_data[[input$pha]])
  } else if (input$pha == "Pct_low_birth_weight_babies_2012_2014") {
    pal_pha <- colorNumeric(c("navy","#99cccc"), domain= pha_shp_data[[input$pha]])
  } else if (input$pha == "Num_ppl_18+_who_disagree_with_accepting_other_cultures") {
    pal_pha <- colorNumeric(c("#000066","#3399ff"), domain= pha_shp_data[[input$pha]])
  } else {
    pal_pha <- colorNumeric(c("#FFFF99", "#C12525"), domain = pha_shp_data[[input$pha]], reverse = FALSE)
  }
  
  
  #creating a proxy map that displays the various stats from the stats drp down 
  leafletProxy("map", data = pha_shp_data) %>%
    clearShapes() %>%
    addPolygons(
      layerId = pha_shp_data$PHA_NAME16,
      fillColor = ~pal_pha(pha_shp_data[[input$pha]]),
      fillOpacity = 0.6,
      weight = 0.6,
      opacity = 1,
      color = "#FFFFFF",
      dashArray = "2",
      label = labels,
      highlight = highlightOptions(
        weight = 4,
        color = "#FFFFFF",
        dashArray = "3",
        fillOpacity = 2,
        bringToFront = FALSE),
      labelOptions = labelOptions(
        style = list("font-weight" = "normal", padding = "3px 5px"),
        textsize = "13px",
        direction = "auto"))#%>%

  #PHIDU
  phidu<-read.csv("data/Logan_PHA_data.csv")
  phidu$X <- NULL #remove cols you dont need
  colnames(phidu)[1]<-"PHA_NAME16"
  phidu_melt<-melt(phidu, idvars="PHA_NAME16")
  
  pha_shp<-readOGR(dsn=path.expand("./data/shape_files"), layer="PHA_2016_AUST_Gen50")
  pha_shp<-pha_shp[pha_shp$PHA_NAME16 %in% c( "Beenleigh/ Eagleby","Bethania - Waterford/ Loganlea/ Waterford West","Boronia Heights - Park Ridge",
                                              "Browns Plains/ Crestmead/ Marsden","Chambers Flat/ Munruben - Park Ridge South",
                                              "Cornubia - Carbrook/ Shailer Park","Daisy Hill/ Springwood","Daisy Hill/ Springwood",
                                              "Edens Landing/ Mount Warren Park/ Wolffdene area","Greenbank","Hillcrest/ Regents Park - Heritage Park",
                                              "Jimboomba/ Logan Village","Kingston/ Slacks Creek","Logan Central/ Woodridge","Loganholme - Tanah Merah",
                                              "Rochedale South - Priestdale/ Underwood"),]
  pha_centre <- data.frame(gCentroid(pha_shp)) 
  centroids <- as.data.frame(coordinates(pha_shp))
  names(centroids) <- c("long", "lat")#capture the lat longs for the centres of each elctoral division
  centroids$PHA_NAME16 <- factor(pha_shp$PHA_NAME16)#add the elect
  
  pha_shp_data<-merge(pha_shp,phidu, by= "PHA_NAME16")
  #pha_melted_shp<-merge(pha_shp, phidu_melt, by = "PHA_NAME16")
  
  #DSS
  dss<-read.csv("data/DSS_SEPT_2018.csv")
  colnames(dss)[1]<-"SA2_NAME16"
  dss_logan<-dss %>% filter (SA2_NAME16 %in% c( "Jimboomba","Marsden","Regents Park - Heritage Park", 
                                              "Crestmead","Rochedale South - Priestdale","Woodridge",
                                              "Eagleby","Greenbank","Loganholme - Tanah Merah","Boronia Heights - Park Ridge",
                                              "Kingston","Loganlea","Slacks Creek","Bethania - Waterford",
                                              "Shailer Park","Edens Landing - Holmview","Browns Plains",
                                              "Hillcrest","Springwood","Waterford West","Underwood","Beenleigh",
                                              "Cornubia - Carbrook","Logan Central","Daisy Hill","Wolffdene - Bahrs Scrub",
                                              "Logan Village","Mount Warren Park", 
                                              "Chambers Flat - Logan Reserve","Munruben - Park Ridge South"))

dss_logan$Age.Pension <-gsub(",", "", dss_logan$Age.Pension)
dss_logan$Family.Tax.Benefit.A <-gsub(",", "", dss_logan$Family.Tax.Benefit.A)
dss_logan$Family.Tax.Benefit.B <-gsub(",", "", dss_logan$Family.Tax.Benefit.B)  
dss_logan$Commonwealth.Seniors.Health.Card <-gsub(",", "", dss_logan$Commonwealth.Seniors.Health.Card)  
dss_logan$Commonwealth.Rent.Assistance..income.units. <-gsub(",", "", dss_logan$Commonwealth.Rent.Assistance..income.units.)  
dss_logan$Pensioner.Concession.Card <-gsub(",", "", dss_logan$Pensioner.Concession.Card)
dss_logan$Newstart.Allowance <-gsub(",", "", dss_logan$Newstart.Allowance)

#change from factor to character so that we can switch to numeric
dss_logan$Age.Pension <-as.character(dss_logan$Age.Pension)
dss_logan$Family.Tax.Benefit.A <-as.character(dss_logan$Family.Tax.Benefit.A)
dss_logan$Family.Tax.Benefit.B <-as.character(dss_logan$Family.Tax.Benefit.B)  
dss_logan$Commonwealth.Seniors.Health.Card <-as.character(dss_logan$Commonwealth.Seniors.Health.Card)  
dss_logan$Commonwealth.Rent.Assistance..income.units. <-as.character(dss_logan$Commonwealth.Rent.Assistance..income.units.)  
dss_logan$Pensioner.Concession.Card <-as.character(dss_logan$Pensioner.Concession.Card)
dss_logan$Low.Income.Card <-as.character(dss_logan$Low.Income.Card)
dss_logan$Carer.Allowance <-as.character(dss_logan$Carer.Allowance)
dss_logan$Carer.Payment <-as.character(dss_logan$Carer.Payment)
dss_logan$Disability.Support.Pension <-as.character(dss_logan$Disability.Support.Pension)
dss_logan$Newstart.Allowance<-as.character(dss_logan$Newstart.Allowance)
#change to numeric
dss_logan$Age.Pension <-as.numeric(dss_logan$Age.Pension)
dss_logan$Family.Tax.Benefit.A <-as.numeric(dss_logan$Family.Tax.Benefit.A)
dss_logan$Family.Tax.Benefit.B <-as.numeric(dss_logan$Family.Tax.Benefit.B)  
dss_logan$Commonwealth.Seniors.Health.Card <-as.numeric(dss_logan$Commonwealth.Seniors.Health.Card)  
dss_logan$Commonwealth.Rent.Assistance..income.units. <-as.numeric(dss_logan$Commonwealth.Rent.Assistance..income.units.)  
dss_logan$Pensioner.Concession.Card <-as.numeric(dss_logan$Pensioner.Concession.Card)
dss_logan$Low.Income.Card <-as.numeric(dss_logan$Low.Income.Card)
dss_logan$Carer.Allowance <-as.numeric(dss_logan$Carer.Allowance)
dss_logan$Carer.Payment <-as.numeric(dss_logan$Carer.Payment)
dss_logan$Disability.Support.Pension <-as.numeric(dss_logan$Disability.Support.Pension)
dss_logan$Newstart.Allowance<-as.numeric(dss_logan$Newstart.Allowance)

dss_logan$SA2_NAME16  <- revalue(dss_logan$SA2_NAME16 , c("Kingston"="Kingston (Qld.)"))

write.csv(dss_logan, file="DSS_LOGAN_SEPT_2018.csv")


#income per sa2
logan_income<-read.csv("data/weekly_income_Logan")
logan_income$Mean<-gsub(",", "",logan_income$Mean)
logan_income$Median<-gsub(",", "",logan_income$Median)

logan_income$Mean<-as.character(logan_income$Mean)
logan_income$Median<-as.character(logan_income$Median)
logan_income$Gini.coefficient<-as.character(logan_income$Gini.coefficient)

logan_income$Mean<-as.numeric(logan_income$Mean)
logan_income$Median<-as.numeric(logan_income$Median)
logan_income$Gini.coefficient<-as.numeric(logan_income$Gini.coefficient)
logan_income$inc_pw <-round(logan_income$inc_pw, 0)

write.csv(logan_income, file="Logan_Income_data_ABS_2016.csv")


#POLICE DATA
crime_num<-read.csv("data/Logan_2016_2019_crime_numbers.csv" )

setDT(crime_num)[, paste0("variable",2:3):= tstrsplit(Month.Year, "-")]
colnames(crime_num)[92]<-"Year"


Year_sum_Assault<-aggregate(crime_num$Assault, by=list(crime_num$Year), sum)
colnames(Year_sum_Assault)[2]<-"Assault"
Year_sum_Sexual_Offences<-aggregate(crime_num$Sexual.Offences, by=list(crime_num$Year), sum)
colnames(Year_sum_Sexual_Offences)[2]<-"Sexual_Offences"
Year_sum_Unlawful_entry_intent<-aggregate(crime_num$Unlawful.Entry.With.Violence...Dwelling, by=list(crime_num$Year), sum)
colnames(Year_sum_Unlawful_entry_intent)[2]<-"Unlawful_Entry_dwelling_with_Violence"
Year_sum_Drug_Offences<-aggregate(crime_num$Drug.Offences, by=list(crime_num$Year), sum)
colnames(Year_sum_Drug_Offences)[2]<-"Drug_Offences"
Year_sum_Breach_DVO<-aggregate(crime_num$Breach.Domestic.Violence.Protection.Order, by=list(crime_num$Year), sum)
colnames(Year_sum_Breach_DVO)[2]<-"Breach_Domestic_Violence_Order"
Year_sum_Public_Nuisance<-aggregate(crime_num$Public.Nuisance, by=list(crime_num$Year), sum)
colnames(Year_sum_Public_Nuisance)[2]<-"Public_Nuissance"
Year_sum_Drink_Driving<-aggregate(crime_num$Drink.Driving, by=list(crime_num$Year), sum)
colnames(Year_sum_Drink_Driving)[2]<-"Drink_Driving"
Year_sum_Disqualified_Driving<-aggregate(crime_num$Disqualified.Driving, by=list(crime_num$Year), sum)
colnames(Year_sum_Disqualified_Driving)[2]<-"Disqual_Driving"


LOGAN_CRIME<-merge(Year_sum_Assault, Year_sum_Sexual_Offences, by="Group.1")
LOGAN_CRIME<-merge(LOGAN_CRIME, Year_sum_Unlawful_entry_intent, by="Group.1")
LOGAN_CRIME<-merge(LOGAN_CRIME, Year_sum_Drug_Offences, by="Group.1")
LOGAN_CRIME<-merge(LOGAN_CRIME, Year_sum_Drink_Driving, by="Group.1")
LOGAN_CRIME<-merge(LOGAN_CRIME, Year_sum_Breach_DVO, by="Group.1")
LOGAN_CRIME<-merge(LOGAN_CRIME, Year_sum_Public_Nuisance, by="Group.1")
LOGAN_CRIME<-merge(LOGAN_CRIME, Year_sum_Disqualified_Driving, by="Group.1")

colnames(LOGAN_CRIME)[1]<-"Year"
LOGAN_CRIME <- LOGAN_CRIME[- 4, ]
df.crime<-melt(LOGAN_CRIME, idvars="Year")



ggplot(data=df.crime)+geom_line(aes(x=Year, y=value, colour=variable, group=variable), size=1.2)  

lga_crime<-read_csv("data/LGA_Reported_Offences_Number.csv", row_number=TRUE)


#read 0-8 sa2 pop data abs 2016
pop<-read.csv("data/0-8_sa2.csv")
colnames(pop)[1]<-"SA2_NAME16"
#filter for only logan sa2s
logan_pop<-pop %>% filter (SA2_NAME16 %in% c( "Jimboomba","Marsden","Regents Park - Heritage Park", 
                                              "Crestmead","Rochedale South - Priestdale","Woodridge",
                                              "Eagleby","Greenbank","Loganholme - Tanah Merah","Boronia Heights - Park Ridge",
                                              "Kingston (Qld.)","Loganlea","Slacks Creek","Bethania - Waterford",
                                              "Shailer Park","Edens Landing - Holmview","Browns Plains",
                                              "Hillcrest","Springwood","Waterford West","Underwood","Beenleigh",
                                              "Cornubia - Carbrook","Logan Central","Daisy Hill","Wolffdene - Bahrs Scrub",
                                              "Logan Village","Mount Warren Park", 
                                              "Chambers Flat - Logan Reserve","Munruben - Park Ridge South"))

write.csv(logan_pop, file="Logan_pop_0_8.csv")
sum(logan_pop$Total_age_0_8)

#read ATSI 0-8 sa2 pop data abs 2016
pop_atsi_0_8<-read.csv("data/0-8_sa2_indig.csv")
colnames(pop_atsi_0_8)[1]<-"SA2_NAME16"
#filter for only logan sa2s
logan_pop_atsi_0_8<-pop_atsi_0_8 %>% filter (SA2_NAME16 %in% c( "Jimboomba","Marsden","Regents Park - Heritage Park", 
                                              "Crestmead","Rochedale South - Priestdale","Woodridge",
                                              "Eagleby","Greenbank","Loganholme - Tanah Merah","Boronia Heights - Park Ridge",
                                              "Kingston (Qld.)","Loganlea","Slacks Creek","Bethania - Waterford",
                                              "Shailer Park","Edens Landing - Holmview","Browns Plains",
                                              "Hillcrest","Springwood","Waterford West","Underwood","Beenleigh",
                                              "Cornubia - Carbrook","Logan Central","Daisy Hill","Wolffdene - Bahrs Scrub",
                                              "Logan Village","Mount Warren Park", 
                                              "Chambers Flat - Logan Reserve","Munruben - Park Ridge South"))

write.csv(logan_pop_atsi_0_8, file="Logan_pop_ATSI_0_8.csv")

#total ATSI pop per sa2
pop_atsi<-read.csv("data/sa2_num_indig.csv")
colnames(pop_atsi)[1]<-"SA2_NAME16"
pop_atsi<-pop_atsi%>% filter (SA2_NAME16 %in% c( "Jimboomba","Marsden","Regents Park - Heritage Park", 
                                                                "Crestmead","Rochedale South - Priestdale","Woodridge",
                                                                "Eagleby","Greenbank","Loganholme - Tanah Merah","Boronia Heights - Park Ridge",
                                                                "Kingston (Qld.)","Loganlea","Slacks Creek","Bethania - Waterford",
                                                                "Shailer Park","Edens Landing - Holmview","Browns Plains",
                                                                "Hillcrest","Springwood","Waterford West","Underwood","Beenleigh",
                                                                "Cornubia - Carbrook","Logan Central","Daisy Hill","Wolffdene - Bahrs Scrub",
                                                                "Logan Village","Mount Warren Park", 
                                                                "Chambers Flat - Logan Reserve","Munruben - Park Ridge South"))

write.csv(pop_atsi, file="Logan_pop_ATSI.csv")

#migration int and os per sa2
mig<-read.csv("data/migration_sa2.csv")
logan_mig<-mig%>% filter (SA2_NAME16 %in% c( "Jimboomba","Marsden","Regents Park - Heritage Park", 
                                                 "Crestmead","Rochedale South - Priestdale","Woodridge",
                                                 "Eagleby","Greenbank","Loganholme - Tanah Merah","Boronia Heights - Park Ridge",
                                                 "Kingston (Qld.)","Loganlea","Slacks Creek","Bethania - Waterford",
                                                 "Shailer Park","Edens Landing - Holmview","Browns Plains",
                                                 "Hillcrest","Springwood","Waterford West","Underwood","Beenleigh",
                                                 "Cornubia - Carbrook","Logan Central","Daisy Hill","Wolffdene - Bahrs Scrub",
                                                 "Logan Village","Mount Warren Park", 
                                                 "Chambers Flat - Logan Reserve","Munruben - Park Ridge South"))

write.csv(logan_mig, file="Logan_migration.csv")

#language other than english per sa2
lang<-read.csv("data/sa2_lang_at_home.csv")
logan_lang<-lang%>% filter (SA2_NAME16 %in% c( "Jimboomba","Marsden","Regents Park - Heritage Park", 
                                             "Crestmead","Rochedale South - Priestdale","Woodridge",
                                             "Eagleby","Greenbank","Loganholme - Tanah Merah","Boronia Heights - Park Ridge",
                                             "Kingston (Qld.)","Loganlea","Slacks Creek","Bethania - Waterford",
                                             "Shailer Park","Edens Landing - Holmview","Browns Plains",
                                             "Hillcrest","Springwood","Waterford West","Underwood","Beenleigh",
                                             "Cornubia - Carbrook","Logan Central","Daisy Hill","Wolffdene - Bahrs Scrub",
                                             "Logan Village","Mount Warren Park", 
                                             "Chambers Flat - Logan Reserve","Munruben - Park Ridge South"))

write.csv(logan_lang, file="Logan_lnag_at_home.csv")

#Median age per sa2
med_age<-read_csv("data/qld_sa2_summary_stats.csv")
logan_med_age<-med_age%>% filter (SA2_NAME16 %in% c( "Jimboomba","Marsden","Regents Park - Heritage Park", 
                                               "Crestmead","Rochedale South - Priestdale","Woodridge",
                                               "Eagleby","Greenbank","Loganholme - Tanah Merah","Boronia Heights - Park Ridge",
                                               "Kingston (Qld.)","Loganlea","Slacks Creek","Bethania - Waterford",
                                               "Shailer Park","Edens Landing - Holmview","Browns Plains",
                                               "Hillcrest","Springwood","Waterford West","Underwood","Beenleigh",
                                               "Cornubia - Carbrook","Logan Central","Daisy Hill","Wolffdene - Bahrs Scrub",
                                               "Logan Village","Mount Warren Park", 
                                               "Chambers Flat - Logan Reserve","Munruben - Park Ridge South"))


logan_med_age$Num_Males<-as.numeric(logan_med_age$Num_Males)
logan_med_age$Num_Females<-as.numeric(logan_med_age$Num_Females)
logan_med_age$Num_Persons<-as.numeric(logan_med_age$Num_Persons)
logan_med_age$`Sex Ratio`<-as.numeric(logan_med_age$`Sex Ratio`)
logan_med_age$Median_age_Males<-as.numeric(logan_med_age$Median_age_Males)
logan_med_age$Median_age_Females<-as.numeric(logan_med_age$Median_age_Females)
logan_med_age$Median_age_Persons<-as.numeric(logan_med_age$Median_age_Persons)

write.csv(logan_med_age, file="Logan_Med_Age.csv")







#~~~~~~~~~~~~~~~~~~~~~~~~combining data for app~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#logan income data abs 2016
dss_logan<-read.csv("data/DSS_LOGAN_SEPT_2018.csv")
logan_income<-read.csv("data/Logan_Income_data_ABS_2016.csv")
logan_income$X <- NULL #remove cols you dont need and to remove duiplicates for merge
logan_pop_0_8<-read.csv("data/Logan_pop_0_8.csv")
logan_pop_0_8$X <- NULL #remove cols you dont need
logan_languages<-read.csv("data/Logan_lnag_at_home.csv")
logan_languages$X <- NULL #remove cols you dont need
logan_migration<-read.csv("data/Logan_migration.csv")
logan_migration$X <- NULL #remove cols you dont need
logan_ATSI_pop<-read.csv("data/Logan_pop_ATSI.csv")
logan_ATSI_pop$X <- NULL #remove cols you dont need
logan_ATSI_pop_0_8<-read.csv("data/Logan_pop_ATSI_0_8.csv")
logan_ATSI_pop_0_8$X <- NULL #remove cols you dont need



#read in shape file for sa2s
sa2<-readOGR(dsn=path.expand("./data/shape_files"), layer="SA2_2016_AUST")
#filter to only have the Sa2s in Logan as per the Logan Togther map
logan_sa2<-sa2[sa2$SA2_NAME16 %in% c( "Jimboomba","Marsden","Regents Park - Heritage Park", 
                                      "Crestmead","Rochedale South - Priestdale","Woodridge",
                                      "Eagleby","Greenbank","Loganholme - Tanah Merah","Boronia Heights - Park Ridge",
                                      "Kingston (Qld.)","Loganlea","Slacks Creek","Bethania - Waterford",
                                      "Shailer Park","Edens Landing - Holmview","Browns Plains",
                                      "Hillcrest","Springwood","Waterford West","Underwood","Beenleigh",
                                      "Cornubia - Carbrook","Logan Central","Daisy Hill","Wolffdene - Bahrs Scrub",
                                      "Logan Village","Mount Warren Park", 
                                      "Chambers Flat - Logan Reserve","Munruben - Park Ridge South"),]
#get the centres of the polygons for labelling purposes
logan_centre <- data.frame(gCentroid(logan_sa2)) 
centroids <- as.data.frame(coordinates(logan_sa2))
names(centroids) <- c("long", "lat")#capture the lat longs for the centres of each elctoral division
centroids$SA2_NAME16 <- factor(logan_sa2$SA2_NAME16)#add the elect_divs to the centroids data  

logan_sa2<-merge(logan_sa2,age, by= "SA2_NAME16")
logan_sa2<-merge(logan_sa2, logan_income, by="SA2_NAME16")
logan_sa2<-merge(logan_sa2,dss_logan, by="SA2_NAME16")
logan_sa2<-merge(logan_sa2,logan_pop_0_8, by="SA2_NAME16")
logan_sa2<-merge(logan_sa2,logan_languages, by="SA2_NAME16")
logan_sa2<-merge(logan_sa2,logan_migration, by="SA2_NAME16")
logan_sa2<-merge(logan_sa2,logan_ATSI_pop, by="SA2_NAME16")
logan_sa2<-merge(logan_sa2,logan_ATSI_pop_0_8, by="SA2_NAME16")
logan_sa2<-merge(logan_sa2, logan_med_age, by="SA2_NAME16")

writeOGR(logan_sa2, ".", "Logan_SA2_service_map", driver="ESRI Shapefile")



observeEvent(input$Type, {
  proxy <- leafletProxy('map')
  
  #Always clear the group first on the observed event 
  proxy %>% clearGroup(group = "1")
  
  #If checked
  if (input$Type =="Early"){
    
    #Filter for the specific group
    df <- filter(services, group == "1")
    
    #Add the specific group's markers
    proxy %>% addMarkers(group = services$group, lng = ~ services$Longitude, lat = ~ services$Latitude)
  }
})

observeEvent(input$Type, {
  proxy <- leafletProxy('map')
  
  #Always clear the group first on the observed event 
  proxy %>% clearGroup(group = "2")
  
  #If checked
  if (input$Type == "Secondary"){
    
    #Filter for the specific group
    df <- filter(services, group == "2")
    
    #Add the specific group's markers
    proxy %>% addMarkers(group = services$group, lng = ~ services$Longitude, lat = ~ services$Latitude)
  }
})

observeEvent(input$Type, {
  proxy <- leafletProxy('map')
  
  #Always clear the group first on the observed event 
  proxy %>% clearGroup(group = "3")
  
  #If checked
  if (input$Type == "Tertiary"){
    
    #Filter for the specific group
    df <- filter(services, group == "3")
    
    #Add the specific group's markers
    proxy %>% addMarkers(group = services$group, lng = ~ services$Longitude, lat = ~ services$Latitude)
  }
})


######



observeEvent(input$Type, {
  leafletProxy("map", data = logan_sa2) %>%
    clearGroup("myMarkers") %>%
    addMarkers(data = services[services$Level_of_support == input$Type, ],
               ~Longitude,
               ~Latitude,
               group = "myMarkers")
})  









#BASE MAP  
output$map<-renderLeaflet({
  
  leaflet(logan_sa2) %>%
    addTiles()%>%
    hideGroup("markers")%>%
    #c("Alcohol & Other Drugs","Child & Family","Domestic & Family Violence","Employment","Finance",
    #   "Health, Social Connection & Wellbeing","Housing & Homelessness", "Information Advice & Referral",
    #  "Legal","Mental & Health","Migrant & Refugee","Sexual Assault & Abuse","Youth"))%>%
    setView(153, -27, zoom = 22)%>%
    
    # Centre the map in the middle of our co-ordinates
    fitBounds(152.8, -27.7, 153.3, -27.6)
})

datFilt <- reactive({
  LevSearch <- paste0(c('xxx',input$Level),collapse = "|")
  LevSearch <- gsub(",","|",LevSearch)
  services[grepl(LevSearch, services$Level_of_support ) & services$Service_Type %in% input$Type]
})



observe({
  if(nrow(datFilt())==0) {
    print("Nothing selected")
    leafletProxy("map") %>%
      clearShapes()
    #clearMarkerClusters()
  } else { #print(paste0("Selected: ", unique(input$InFlags&input$InFlags2)))
    print("Something Selected")
    leafletProxy("map", data=datFilt()) %>% 
      #clearMarkerClusters() %>% 
      addCircleMarkers(group="markers", lng=~services$Longitude, lat=~services$Latitude,
                       #clusterOptions=markerClusterOptions(), weight=3,
                       color="#33CC33", opacity=1, fillColor="#FF9900", 
                       fillOpacity=0.8)
  }
})





level <- reactive({
  services %>% filter(Level_of_support == input$Level)
})  

level_react <- reactive({if(input$Level == "Early") {
  level()
}
  else if(input$Level== "Secondary") {
    level()
  }
  else {
    
    level() %>% filter(Level_of_support %in% input$Level)
  }
})



income<-read.csv("data/weekly_income_Logan")
head(income)

lga_cars<-read.csv("data/Logan_num_of_cars_LGA.csv")


#Service Type Data
Alcohol_Drugs<-read.csv("data/Alcohol_Drugs.csv")
Child_Family<-read.csv("data/Child_Family.csv")
Domestic_Family_Violence<-read.csv("data/Domestic_Family_Violence.csv")
Employment<-read.csv("data/Employment.csv")
Finance<-read.csv("data/Finance.csv")
Health_Social_Connection_Wellbeing<-read.csv("data/Health_Social_Connection_Wellbeing.csv")
Housing_Homelessness<-read.csv("data/Housing_Homelessness.csv")
Information_Advice_Referral<-read.csv("data/Information_Advice_Referral.csv")
Legal<-read.csv("data/Legal.csv")
Mental_Health<-read.csv("data/Mental_Health.csv")
Migrant_Refugee<-read.csv("data/Migrant_Refugee.csv")
Sexual_Assault_Abuse<-read.csv("data/Sexual_Assault_Abuse.csv")
Youth<-read.csv("data/Youth.csv")




#SOcial housing register data
housing_reg<-read.csv("data/2018-social-housing-register-data-file.csv")
LoganHousing<-housing_reg[housing_reg$LGA_Full.Name =="Logan City Council",]

LoganHousing$Timeon_housingregister<-as.character(LoganHousing$Timeon_housingregister)
LoganHousing$Timeon_housingregister<-as.numeric(LoganHousing$Timeon_housingregister)

ggplot(data=LoganHousing,aes(Timeon_housingregister,colour=HR_Segment, shape=ApprovalStatus))+geom_point(stat="count")




LoganHousing$ApplicationDate <- as.Date(LoganHousing$ApplicationDate , format="%d/%m/%Y")
LoganHousing<-separate(LoganHousing, "ApplicationDate", c("Year", "Month", "Day"), sep = "-")

LoganHousing %>% mutate_all(funs(sum), na.rm = TRUE) %>%
  gather(key=ApplicationDate, value=Number) %>% 
  


#HR segment social housing
p<-ggplot(data=LoganHousing, aes(x=Timeon_housingregister,fill=subset[LoganHousing$Year == 2018,]))+geom_bar()
p

ggplot(LoganHousing, aes(x=HR_Segment, fill=Timeon_housingregister ))+geom_bar()


#Logan scatter plot
Logan_Count<-LoganHousing %>% 
  filter(!is.na(LGA_Full.Name)) %>% 
  group_by(Year) %>% 
  count()
Logan_Count$n<-as.numeric(Logan_Count$n)

Logan_melt<-melt(LoganHousing)

ggplot(Logan_melt)+ geom_point(aes(x=Year, y=value, colour=HR_Segment))




#____________________________________________________

#logan_languages$X <- NULL #remove cols you dont need
#BAR CHART LANGUAGE DATA
df.lang<-logan_languages#read
df.lang$Total<-NULL 
colnames(df.lang)[2]<-"N.Euro"
colnames(df.lang)[3]<-"S.Euro"
colnames(df.lang)[4]<-"E.Euro"
colnames(df.lang)[5]<-"SW.Asian"
colnames(df.lang)[6]<-"S.Asian"
colnames(df.lang)[7]<-"SE.Asian"
colnames(df.lang)[8]<-"E.Asian"
colnames(df.lang)[9]<-"Aus.Indig"

df.lang<-melt(df.lang, idvars="SA2_NAME16")#melt into long format data frame for graphs

df.lang<-within(df.lang,
                variable<-factor(variable,
                                 levels=c("Aus.Indig","E.Asian","SE.Asian","S.Asian","SW.Asian",
                                          "E.Euro","S.Euro","N.Euro")))
#df for percentage graphs
logan_languages_pct<-logan_languages #read

#divide to create percentages per language category
logan_languages_pct$Aus.Indig<-(logan_languages_pct$Australian.Indigenous.Languages/logan_languages_pct$Total)*100
logan_languages_pct$E.Asian <-(logan_languages_pct$Eastern.Asian.Languages /logan_languages_pct$Total)*100
logan_languages_pct$SE.Asian<-(logan_languages_pct$Southeast.Asian.Languages /logan_languages_pct$Total)*100
logan_languages_pct$S.Asian<-(logan_languages_pct$Southern.Asian.Languages /logan_languages_pct$Total)*100
logan_languages_pct$SW.Asian<-(logan_languages_pct$Southwest.and.Central.Asian.Languages /logan_languages_pct$Total)*100
logan_languages_pct$N.Euro<-(logan_languages_pct$Northern.European.Languages /logan_languages_pct$Total)*100
logan_languages_pct$E.Euro<-(logan_languages_pct$Eastern.European.Languages /logan_languages_pct$Total)*100
logan_languages_pct$S.Euro<-(logan_languages_pct$Southern.European.Languages /logan_languages_pct$Total)*100
logan_languages_pct[,2:10]<-NULL



df.lang.pct<-logan_languages_pct #save as new object


df.lang.pct<-melt(df.lang.pct, idvars="SA2_NAME16")#melt into long format data frame for graphs
df.lang.pct<-within(df.lang.pct,
                    variable<-factor(variable,
                                     levels=c("Aus.Indig","E.Asian","SE.Asian","S.Asian","SW.Asian",
                                              "E.Euro","S.Euro","N.Euro")))
df.lang.pct$value<-round(df.lang.pct$value, digits = 2) #round decimal places
                               


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
library(ggmap)
#get lat long coordinates for addresses in ACEQUA data set
ACEQUA<-read.csv("Education-services-qld-export_Acequa.csv",stringsAsFactors = FALSE)

ACEQUA<-ACEQUA %>% filter(Suburb %in% c("BAHRS SCRUB","BANNOCKBURN","BEENLEIGH","BELIVAH","BERRINBA",
                                        "BETHANIA","BORONIA HEIGHTS","BROWNS PLAINS","BUCCAN","CARBROOK",
                                        "CEDAR CREEK","CEDAR GROVE","CEDAR VALE","CHAMBERS FLAT",
                                        "CORNUBIA","CRESTMEAD","DAISY HILL","EAGLEBY","EDENS LANDING",
                                        "FLAGSTONE","FLINDERS LAKES","FORESTDALE","GLENLOGAN","GREENBANK",
                                        "HERITAGE PARK","HILLCREST","HOLMVIEW","JIMBOOMBA","KAGARU",
                                        "KAIRABAH","KINGSTON","LOGAN CENTRAL","LOGAN RESERVE","LOGAN VILLAGE",
                                        "LOGANHOLME","LOGANLEA","LYONS","MACLEAN (NORTH & SOUTH)","MARSDEN",
                                        "MEADOWBROOK","MONARCH GLEN","MOUNT WARREN PARK","MUNDOOLUN",
                                        "MUNRUBEN","NEW BEITH","PARK RIDGE","PARK RIDGE SOUTH",
                                        "PRIESTDALE","REGENTS PARK","RIVERBEND","ROCHEDALE SOUTH",
                                        "SHAILER PARK","SILVERBARK RIDGE","SLACKS CREEK",
                                        "SPRINGWOOD","STOCKLEIGH","TAMBORINE","TANAH MERAH",
                                        "UNDERWOOD","UNDULLAH","VERESDALE","VERESDALE SCRUB",
                                        "WATERFORD","WATERFORD WEST","WINDAROO","WOLFFDENE",
                                        "WOODHILL","WOODRIDGE","YARRABILBA"))

#merge address fields into one field for geocoding 
ACEQUA %<>%
  unite(Merged_Address, ServiceAddress, Suburb,State, remove = FALSE)

ACEQUA$Merged_Address<-gsub("_", ",",ACEQUA$Merged_Address)

write.csv(ACEQUA, file="ACEQUA_Logan_2018.csv")

geocodes <- geocode(as.character(ACEQUA$Merged_Address))

geocoded <- data.frame(stringsAsFactors = FALSE)

library(googleAuthR)
library(mapsapi)
# Loop through the addresses to get the latitude and longitude of each address and add it to the
# origAddress data frame in new columns lat and lon
for(i in 1:nrow(ACEQUA))
{
  # Print("Working...")
  result <- geocode(ACEQUA$Merged_Address[i], output = "latlon", source = "google")
  ACEQUA$lon[i] <- as.numeric(result[1])
  ACEQUA$lat[i] <- as.numeric(result[2])
  #ACEQUA$geoAddress[i] <- as.character(result[3])
}



